// import { StyleSheet } from 'react-native'

export default {
  color: '#ffffff',
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding: 15
  },
  button: {
    backgroundColor: '#cc5500',
    padding: 10,
    marginTop: 20,
    borderRadius: 3
  },
  icon: {
    width: 30,
    height: 30
  },
  search: {
    height: 40,
    borderWidth: 0,
    borderRadius: 3,
    padding: 5,
    marginTop: 20,
    backgroundColor: '#fff'
  },
  title: {
    color: '#fff',
    fontSize: 38,
    textAlign: 'center',
    paddingBottom: 100,
    fontWeight: 'bold'
  },
  text: {
    textAlign: 'center',
    paddingTop: 20
  },
  header: {
    backgroundColor: '#02131a'
  },
  headerTitle: {
    color: '#cc5500',
  }
}