import React from 'react'
import { StyleSheet } from 'react-native'

import Search from './views/Search'
import About from './views/About'

import { TabNavigator } from 'react-navigation'

const Tabs = TabNavigator({
  Search: { screen: Search },
  About: { screen: About}
}, {
  tabBarOptions: {
    showIcon: true,
    showLabel: false,
    indicatorStyle: {
      height: 2,
      backgroundColor: '#ffffff'
    },
    labelStyle: {
      color: '#cc5500',
      fontSize: 20,
    },
    style: {
      backgroundColor: '#02131a',
      borderTopWidth: 2,
      borderColor: '#cc5500'
    }
  }
})

export default class App extends React.Component {
  render() {
    return (
      <Tabs />
    )
  }
}


