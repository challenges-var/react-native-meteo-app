import React from 'react'
import propTypes from 'prop-types';
import { StyleSheet, View, Text, Image } from 'react-native'

import moment from 'moment'

import Animation from '../Animation'

export default class Row extends React.Component {

  static propTypes = {
    day: propTypes.object,
    index: propTypes.number
  }

  day () {
    let day = moment(this.props.day.dt * 1000).format('ddd')
    return (
      <Text style={styles.day}>{ day.toUpperCase()}</Text>
    )
  }

  date () {
    let day = moment(this.props.day.dt * 1000).format('Do')
    return (
      <Text>{ day }</Text>
    )
  }

  hour () {
    let day = moment(this.props.day.dt * 1000).format('h:mm a')
    return (
      <Text>{ day.toUpperCase() }</Text>
    )
  }

  icon (size) {
    const type = this.props.day.weather[0].main.toLowerCase()
    let image
    switch (type) {
      case 'clouds':
        image = require('../../icons/cloud.png')
        break
      case 'rain':
        image = require('../../icons/rain.png')
        break
      default:
        image = require('../../icons/sun.png')
    }
    return (
      <View>
        <Image source={image} style={{height: size, width: size}} />
      </View>
    )
  }

  render() {
    if (this.props.index === 0) {
      return (
        <Animation delay={this.props.index * 50}>
          <View style={styles.first}>
            { this.icon(70) }
            <View style={styles.flex}>
              <Text style={styles.dateFirst}>{this.date()}</Text>
              <Text style={styles.dayFirst}>{this.day()}</Text>
              <Text style={styles.minMax}>min {Math.round(this.props.day.main.temp_min)}˚C - max {Math.round(this.props.day.main.temp_max)}˚C</Text>
              <Text style={styles.firstDesc}>{ this.props.day.weather[0].description.toUpperCase() }</Text>
            </View>
            <View> 
              <Text style={styles.dateFirst}>{this.hour()}</Text>
              <Text style={styles.tempFirst}>{Math.round(this.props.day.main.temp)}˚C</Text>
            </View>
          </View>
        </Animation>
      )
    } else {
      return (
        <Animation delay={this.props.index * 50}>
          <View style={styles.view}>
            { this.icon(40) }
            <View style={styles.flex}>
              <Text style={styles.date}>{this.date()}</Text>
              <Text style={styles.day}>{this.day()}</Text>
              <Text style={styles.minMax}>min {Math.round(this.props.day.main.temp_min)}˚C - max {Math.round(this.props.day.main.temp_max)}˚C</Text>
              <Text style={styles.desc}>{ this.props.day.weather[0].description.toUpperCase() }</Text>
            </View>
            <View> 
              <Text style={styles.date}>{this.hour()}</Text>
              <Text style={styles.temp}>{Math.round(this.props.day.main.temp)}˚C</Text>
            </View>
          </View>
        </Animation>
      )
    }
  }
}

const styles = StyleSheet.create({
  view: {
    backgroundColor: '#fff',
    borderWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: 'lightblue',
    paddingHorizontal: 15,
    paddingVertical: 25,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  date: {
    color: 'salmon',
    fontSize: 14,
  },
  temp: {
    color: '#434380',
    fontSize: 22,
    fontWeight: 'bold'
  },
  day: {
    color: '#000',
    fontSize: 22,
    fontWeight: 'bold'
  },
  first: {
    backgroundColor: '#9999ff',
    borderWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: 'lightblue',
    paddingHorizontal: 15,
    paddingVertical: 25,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  dateFirst: {
    color: '#fff',
  },
  dayFirst: {
    color: '#fff',
  },
  tempFirst: {
    color: '#434380',
    fontSize: 32,
    fontWeight: 'bold'
  },
  minMax: {
    fontSize: 12
  },
  flex: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  desc: {
    fontSize: 10,
    fontWeight: 'bold',
    color: 'orange'
  },
  firstDesc: {
    fontSize: 10,
    fontWeight: 'bold',
    color: 'lightgrey'
  }
})