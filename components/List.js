import React from 'react'
import { Text, ActivityIndicator, ListView, Image } from 'react-native'
import axios from 'axios'

import style from '../styles/mainStyle.js'

import WeatherRow from './weather/Row'

export default class List extends React.Component {

  static navigationOptions = ({navigation}) => {
    console.log(navigation)
    return {
      title: `${navigation.state.params.city.toUpperCase()}`,
      tabBarIcon: () => {
        return <Image source={require('../icons/home.png')} style={style.icon}/>
      }
    }
  }

  constructor(props){
    super(props)
    console.log('state', this.props.navigation.state)
    this.state= {
      city: this.props.navigation.state.params.city,
      report: null
    }
    setTimeout(() => {
      this.fetchWeather()
    }, 1000)
  }

  fetchWeather () {
    const city = this.state.city
    const days = 80
    const apiKey = '19f71045eef19f890de3376ac5b00f62'
    axios.get(`http://api.openweathermap.org/data/2.5/forecast?q=${city}&mode=json&units=metric&cnt=${days}&APPID=${apiKey}`)
    .then((res) => {
      console.log(res.data)
      this.setState({report: res.data})
    })
  }

  render() {
    if ( !this.state.report ) {
      return (
        <ActivityIndicator color="black" size="large" style={{flex: 1}}/>
      )
    } else {
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

      return (
        <ListView
         dataSource={ds.cloneWithRows(this.state.report.list)}
         renderRow={(row, j, k) => <WeatherRow day={row} index={parseInt(k, 10)}/> } 
         />
      )
    }
  }
}