import React from 'react'
import { StyleSheet, View, Image, TextInput, Button, Text } from 'react-native'

import { StackNavigator } from 'react-navigation'

import List from '../components/List'

import style from '../styles/mainStyle.js'



class Search extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      city: ''
    }
  }

  setCity (city) {
    this.setState({city})
  }

  search () {
    console.log('hello')
  }

  submit () {
    this.props.navigation.navigate('Result', {city: this.state.city})
  }

  static navigationOptions = {
    title: 'TIMOTEMP',
    tabBarIcon: () => {
      return <Image source={require('../icons/home.png')} style={style.icon}/>
    }
  }
  
  render() {

    return (
      <View style={style.container}>
        <Image
          style={{
            flex: 1,
            position: 'absolute',
            width: '115%',
            height: '115%',
            justifyContent: 'center'
          }}
          source={require('../assets/back.png')} />
        
          <View>
            <Text style={style.title}>Hello Dawg!</Text>
            <Text style={style.text}>Welcome to probably the best react native app! :)</Text>
          </View>

          <View>
            <TextInput
              style={style.search}
              onChangeText={(text) => this.setCity(text)}
              onSubmitEditing={() => this.submit()}
              value={this.state.city}
            />
            <View style={style.button}>
              <Button color='#ffffff' onPress={() => this.submit()} title='Search City' />
            </View>
          </View>
      </View>
    )
  }
}



const navigationOptions = {
  headerStyle: style.header,
  headerTitleStyle: style.headerTitle
}

export default StackNavigator({
  Search: {
    screen: Search,
    navigationOptions
  },
  Result: {
    screen: List,
    navigationOptions
  }
})