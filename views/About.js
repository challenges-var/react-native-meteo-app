import React from 'react'
import { StyleSheet, Text, View, Image, Button } from 'react-native'

import style from '../styles/mainStyle.js'

export default class About extends React.Component {

  static navigationOptions = {
    tabBarIcon: () => {
      return <Image source={require('../icons/about.png')} style={styles.icon}/>
    }
  }

  search () {
    this.props.navigation.navigate('Search')
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>About</Text>
        <Text>Le Lorem Ipsum est simplement du faux texte employé dans la composition</Text>
        <View style={style.button}>
          <Button color='#ffffff' onPress={() => this.search()} title='Back' style={style.button}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding: 15
  },
  icon: {
    marginTop: 5,
    width: 35,
    height: 30
  }
})